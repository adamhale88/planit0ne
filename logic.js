let currentTime = moment().format("kk:mm");
let now = moment().format("dddd, MMMM Do YYYY");
$(".time-display").text(now);
// // ---------time-block date/time values-------------//

timeBlocks = {
  nineAm: {
    time: "09:00",

    timeEnd: "09:59",

    textArea: $("#text1"),

    timeBlockId: 1,
  },

  tenAm: {
    time: "10:00",

    timeEnd: "10:59",

    textArea: $("#text2"),

    timeBlockId: 2,
  },

  elevenAm: {
    time: "11:00",

    timeEnd: "11:59",

    textArea: $("#text3"),

    timeBlockId: 3,
  },

  twelvePm: {
    time: "12:00",

    timeEnd: "12:59",

    textArea: $("#text4"),

    timeBlockId: 4,
  },

  onePm: {
    time: "13:00",

    timeEnd: "13:59",

    textArea: $("#text5"),

    timeBlockId: 5,
  },

  twoPm: {
    time: "14:00",

    timeEnd: "14:59",

    textArea: $("#text6"),

    timeBlockId: 6,
  },

  threePm: {
    time: "15:00",

    timeEnd: "15:59",

    textArea: $("#text7"),

    timeBlockId: 7,
  },

  fourPm: {
    time: "16:00",

    timeEnd: "16:59",

    textArea: $("#text8"),

    timeBlockId: 8,
  },

  fivePm: {
    time: "17:00",

    timeEnd: "17:59",

    textArea: $("#text9"),

    timeBlockId: 9,
  },
};

$("#time1") === timeBlocks["nineAm"].time;
$("#time2") === timeBlocks["tenAm"].time;
$("#time3") === timeBlocks["twelvePm"].time;
$("#time5") === timeBlocks["onePm"].time;
$("#time6") === timeBlocks["twoPm"].time;
$("#time7") === timeBlocks["threePm"].time;
$("#time8") === timeBlocks["fourPm"].time;
$("#time9") === timeBlocks["fivePm"].time;

for (let timeBlock in timeBlocks) {
  const timeStorage = localStorage;
  if (
    currentTime > timeBlocks[timeBlock].time &&
    currentTime > timeBlocks["nineAm"].time
  ) {
    timeBlocks[timeBlock].textArea[0].classList.remove = "future";
    timeBlocks[timeBlock].textArea[0].className = "past form-control";
  }

  if (
    currentTime >= timeBlocks[timeBlock].time &&
    currentTime <= timeBlocks[timeBlock].timeEnd
  ) {
    timeBlocks[timeBlock].textArea[0].classList.remove = "future";
    timeBlocks[timeBlock].textArea[0].className = "present form-control";
  }

  if (timeStorage[timeBlocks[timeBlock].timeBlockId]) {
    timeBlocks[timeBlock].textArea[0].value =
      timeStorage[timeBlocks[timeBlock].timeBlockId];
  }

  if (currentTime === "00:00:00") {
    timeStorage.clear();
  }
}

const btn1 = $("#save1");
$(btn1).on("click", (e) => {
  localStorage.setItem(1, $("#text1")[0].value);
});

const btn2 = $("#save2");
$(btn2).on("click", (e) => {
  localStorage.setItem(2, $("#text2")[0].value);
});

const btn3 = $("#save3");
$(btn3).on("click", (e) => {
  localStorage.setItem(3, $("#text3")[0].value);
});

const btn4 = $("#save4");
$(btn4).on("click", (e) => {
  localStorage.setItem(4, $("#text4")[0].value);
});

const btn5 = $("#save5");
$(btn5).on("click", (e) => {
  localStorage.setItem(5, $("#text5")[0].value);
});

const btn6 = $("#save6");
$(btn6).on("click", (e) => {
  localStorage.setItem(6, $("#text6")[0].value);
});

const btn7 = $("#save7");
$(btn7).on("click", (e) => {
  localStorage.setItem(7, $("#text7")[0].value);
});

const btn8 = $("#save8");
$(btn8).on("click", (e) => {
  localStorage.setItem(8, $("#text8")[0].value);
});

const btn9 = $("#save9");
$(btn9).on("click", (e) => {
  localStorage.setItem(9, $("#text9")[0].value);
});
