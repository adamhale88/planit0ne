# 📖 "Planit One"

`Your 9-5 work day memos`

* Never miss another bussiness meeting again!  

* Forgot to call Marty back last week? Get it on the Planner! 

* Gotta get those reports in by noon? Remind youself! . 

## 🏆 "https://adamhale88.github.io/planit0ne/" 🏆

 &nbsp;  
<img src="images\screenshot.png"/>
 &nbsp;  

## Languages:
---
| - `HTML` - | -  `CSS` -  | -  `Javascript` - 
|
&nbsp;   

## 📝 Features:
 [Moment.js](https://momentjs.com/)

&nbsp;

## Acceptance Criteria
```md
GIVEN I am using a daily planner to create a schedule
WHEN I open the planner
THEN the current day is displayed at the top of the calendar
WHEN I scroll down
THEN I am presented with timeblocks for standard business hours
WHEN I view the timeblocks for that day
THEN each timeblock is color coded to indicate whether it is in the past, present, or future
WHEN I click into a timeblock
THEN I can enter an event
WHEN I click the save button for that timeblock
THEN the text for that event is saved in local storage
WHEN I refresh the page
THEN the saved events persist
```
&nbsp;

## 🏆 Project Criteria
- [X] Uses a date utility library to work with date and time

- [X] Current day is displayed at the top of the calendar

- [X] Time-Blocks for standard business hours

- [X] Each timeblock is color coded to indicate whether it is in the past, present, or future

- [X] Time-Block save button holds input in local storage.

- [x] data persist

- [X] Site adapts to various viewport screens and devices 

&nbsp;

## Contribute @

Feel free to open issues with feed back and or clone down and help continue improvements at `https://github.com/AdamHale88/planit0ne.git`